const employeesData = require("../js_drill_2.cjs");
const salaryOf10000 = require("../Problem3.js");
try {
  const salaryFactor = salaryOf10000.salaryIsFactorOf10000(
    employeesData.employees
  );
  if (
    salaryFactor === null ||
    salaryFactor.length === 0 ||
    salaryFactor === undefined
  ) {
    throw new Error("Data Not Found");
  }
  console.log(`${JSON.stringify(salaryFactor)}`);
} catch (error) {
  console.log(error);
}
