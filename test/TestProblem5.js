const employeesData = require("../js_drill_2.cjs");
const salariesBasedOnCountry = require("../Problem5.js");
try {
  const sumCountrySalary = salariesBasedOnCountry.sumSalariesBasedOnCountry(
    employeesData.employees
  );
  if (
    sumCountrySalary === null ||
    sumCountrySalary === undefined ||
    sumCountrySalary.length === 0
  ) {
    throw new Error("Data not found");
  }
  console.log(`${JSON.stringify(sumCountrySalary)}`);
} catch (error) {
  console.log(error);
}
