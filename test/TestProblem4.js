const employeesData = require("../js_drill_2.cjs");
const totalSalary = require("../Problem4.js");

try {
  const salary = totalSalary.sumOfAllSalaries(employeesData.employees);
  if (salary === null || salary === undefined) {
    throw new Error("data is null or undefined");
  }
  console.log(`${salary}`);
} catch (error) {
  console.log(error);
}
