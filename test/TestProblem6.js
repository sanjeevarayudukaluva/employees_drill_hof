const employeesData = require("../js_drill_2.cjs");
const averageByCountry = require("../Problem6.js");
try {
  const averageSalaryOfEachCountry = averageByCountry.averageSalaryBasedCountry(
    employeesData.employees
  );
  if (
    averageSalaryOfEachCountry === null ||
    averageSalaryOfEachCountry === undefined ||
    averageSalaryOfEachCountry.length === 0
  ) {
    throw new Error("Data is null or Undefined");
  }
  console.log(`${JSON.stringify(averageSalaryOfEachCountry)}`);
} catch (error) {
  console.log(error);
}
