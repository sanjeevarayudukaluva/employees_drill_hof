const employeesData = require("../js_drill_2.cjs");
const convertedSalary = require("../Problem2.js");

try {
  const salary = convertedSalary.convertSalaryToNumber(employeesData.employees);
  if (salary === null || salary === undefined) {
    throw new Error(`data is null or undefined`);
  }
  console.log(`${JSON.stringify(salary)}`);
} catch (error) {
  console.log(error);
}
