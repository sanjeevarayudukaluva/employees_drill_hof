//Find the sum of all salaries.

function sumOfAllSalaries(data) {
  if (Array.isArray(data) && data.length !== 0) {
    let totalSalary = 0;
    return data.reduce((sum, eachObject) => {
      const salaryNumber = parseFloat(eachObject.salary.slice(1));

      return sum + salaryNumber;
    }, 0);
  } else {
    return null;
  }
}
module.exports = { sumOfAllSalaries };
// const emp=require("./js_drill_2.cjs");
// console.log(`${JSON.stringify(emp)}`);
