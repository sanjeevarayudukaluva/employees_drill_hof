//Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )
function allWebDelopers(data) {
  if (Array.isArray(data) && data.length !== 0) {
    return data.filter((eachObject) =>
      eachObject.job.includes("Web Developer")
    );
  } else {
    return null;
  }
}

module.exports = { allWebDelopers };
