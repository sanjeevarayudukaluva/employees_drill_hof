// Convert all the salary values into proper numbers instead of strings.

function convertSalaryToNumber(data) {
  if (Array.isArray(data) && data.length !== 0) {
    return data.map((eachObject) => {
      eachObject.salary = parseFloat(eachObject.salary.replace("$", ""));
      return eachObject;
    });
  } else {
    return null;
  }
}
module.exports = { convertSalaryToNumber };
