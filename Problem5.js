// Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).

function sumSalariesBasedOnCountry(data) {
  if (Array.isArray(data) && data.length !== 0) {
    const salariesBasedOnCountry = {};
    data.forEach((element) => {
      let salary = parseFloat(element.salary.slice(1));
      if (salariesBasedOnCountry.hasOwnProperty(element.location)) {
        salariesBasedOnCountry[element.location] += salary;
      } else {
        salariesBasedOnCountry[element.location] = salary;
      }
    });
    return salariesBasedOnCountry;
  } else {
    return null;
  }
}
module.exports = { sumSalariesBasedOnCountry };
