// Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

function salaryIsFactorOf10000(data) {
  if (Array.isArray(data) && data.length != 0) {
    return data.map((eachObject) => {
      let salaryNumber = parseFloat(eachObject.salary.slice(1));
      let corrected_salary = salaryNumber * 10000;
      eachObject.salary = corrected_salary;
      return eachObject;
    });
  } else {
    return null;
  }
}
module.exports = { salaryIsFactorOf10000 };
