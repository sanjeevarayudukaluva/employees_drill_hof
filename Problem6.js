// Find the average salary of based on country. ( Groupd it based on country and then find the average )

function averageSalaryBasedCountry(data) {
  if (Array.isArray(data) && data.length !== 0) {
    const averageSalaryCountry = {};
    data.forEach((element) => {
      let salary = parseFloat(element.salary.slice(1));
      if (averageSalaryCountry.hasOwnProperty(element.location)) {
        averageSalaryCountry[element.location].sum += salary;
        averageSalaryCountry[element.location].count++;
      } else {
        averageSalaryCountry[element.location] = { sum: salary, count: 1 };
      }
    });
    const averageByCountry = {};
    for (let country in averageSalaryCountry) {
      averageByCountry[country] =
        averageSalaryCountry[country].sum / averageSalaryCountry[country].count;
    }
    return averageByCountry;
  } else {
    return null;
  }
}
module.exports = { averageSalaryBasedCountry };
